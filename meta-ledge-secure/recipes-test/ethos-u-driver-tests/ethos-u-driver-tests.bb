DESCRIPTION = "Functional test for the Ethos-U NPU driver"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://ethos-u-driver-tests.py"

do_install() {
    install -D -p -m 0755 ${UNPACKDIR}/ethos-u-driver-tests.py ${D}${bindir}/ethos-u-driver-tests.py
}

RDEPENDS:${PN} = "python3-core python3-fcntl"
