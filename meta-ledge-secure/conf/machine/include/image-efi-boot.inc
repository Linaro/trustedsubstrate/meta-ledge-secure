# SPDX-License-Identifier: MIT

IMAGE_EFI_BOOT_FILES += "default.pcr.signature"
IMAGE_EFI_BOOT_FILES += "kernel515.pcr.signature"
IMAGE_EFI_BOOT_FILES += "ledge-initramfs-${MACHINE}.rootfs.cpio.gz"

XEN_IMAGE_EFI_BOOT_FILES = "${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
                                              'xen-${MACHINE}.efi.signed;xen.efi xen.cfg', '', d)}"
XEN_IMAGE_EFI_BOOT_FILES:ledge-qemuarm = ""
IMAGE_EFI_BOOT_FILES += "${XEN_IMAGE_EFI_BOOT_FILES}"

do_image_wic[depends] += "${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
                                               'xen:do_deploy', '', d)}"

ROOT_PART_UUID = "f3374295-b635-44af-90b6-3f65ded2e2e4"
ROOT_FS_UUID = "6091b3a4-ce08-3020-93a6-f755a22ef03b"
WKS_ROOTFS_PART_EXTRA_ARGS = "--uuid=${ROOT_PART_UUID} --fsuuid=${ROOT_FS_UUID}"

GRUB_CFG_FILE = "${S}/grub.cfg"

do_image_wic[prefuncs] += "generate_grub_cfg"

generate_grub_cfg() {
	cat <<-'EOF' > "${GRUB_CFG_FILE}"
	set term="vt100"
	set default="0"
	set timeout="5"

	kernel_cmdline="rootwait rw panic=60"

	menuentry '${DISTRO_NAME}' {
	        echo 'Loading Linux ...'
	        linux /${KERNEL_IMAGETYPE} $kernel_cmdline root=UUID=@ROOT_FS_UUID@ @OSTREE_ARG@
	        echo 'Loading initial ramdisk ...'
	        initrd /ledge-initramfs-${MACHINE}.rootfs.cpio.gz
	}
	EOF

	if [ "${@bb.utils.contains('DISTRO_FEATURES', 'xen', '1', '0', d)}" = "1" ] ; then
		cat <<-'EOF' >> "${GRUB_CFG_FILE}"

		menuentry '${DISTRO_NAME} - Xen (if supported)' {
		        chainloader /xen.efi
		}
		EOF
	fi

	if [ "${@bb.utils.contains("DISTRO_FEATURES", "sota", "true", "false", d)}" = "true" ] ; then
		# Extract ostree= kernel argument
		OSTREE_ARG=""
		for arg in $(grep ostree= "${OTA_SYSROOT}/boot/loader.1/grub.cfg"); do
			case "${arg}" in
				ostree=*)
					OSTREE_ARG="${arg}"
					;;
			esac
		done
	fi

	# Replace placeholders in the generated grub config
	sed -i -e "s|@ROOT_FS_UUID@|${ROOT_FS_UUID}|" \
			-e "s|@OSTREE_ARG@|${OSTREE_ARG}|" \
			${GRUB_CFG_FILE}
}
