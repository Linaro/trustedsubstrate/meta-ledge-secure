# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://0001-arm-acpi-don-t-expose-the-ACPI-IORT-SMMUv3-entry-to-.patch"
SRC_URI += "file://acpi.cfg"
SRC_URI += "file://msi.cfg"
SRC_URI += "file://xen.cfg.in"
SRC_URI += "file://uefi-certificates/db.key"
SRC_URI += "file://uefi-certificates/db.crt"

DEPENDS += "coreutils-native "
DEPENDS += "e2fsprogs-native"
DEPENDS += "efitools-native"
DEPENDS += "gettext-native"
DEPENDS += "sbsigntool-native"

SBSIGN_KEY = "${UNPACKDIR}/uefi-certificates/db.key"
SBSIGN_CERT = "${UNPACKDIR}/uefi-certificates/db.crt"

DOM0_MEMORY_SIZE ??= "4096"

inherit image-efi-boot

do_compile:append:aarch64() {
    ${STAGING_BINDIR_NATIVE}/sbsign \
        --key ${SBSIGN_KEY} \
        --cert ${SBSIGN_CERT} \
        ${B}/xen/xen.efi \
        --output ${B}/xen/xen.efi.signed
}

do_deploy:append() {
    export DOM0_MEMORY_SIZE="${DOM0_MEMORY_SIZE}M"
    export ROOT_FS_UUID="${ROOT_FS_UUID}"
    export MACHINE="${MACHINE}"

    envsubst < ${UNPACKDIR}/xen.cfg.in > ${DEPLOYDIR}/xen.cfg

    if [ -f ${B}/xen/xen.efi.signed ]; then
        install -m 0644 ${B}/xen/xen.efi.signed ${DEPLOYDIR}/xen-${MACHINE}.efi.signed
    fi
}
