FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://grub-initial.cfg \
            file://0001-verifiers-Don-t-return-error-for-deferred-image.patch \
            file://0001-grub-efi-modinfo.sh.in-remove-build-environment-deta.patch \
"

SRC_URI += "file://uefi-certificates/db.key"
SRC_URI += "file://uefi-certificates/db.crt"

GRUB_BUILDIN += "echo pgp gcry_sha512 gcry_rsa password_pbkdf2 echo all_video \
                search_fs_uuid reboot sleep"

DEPENDS += "sbsigntool-native"

SBSIGN_KEY = "${UNPACKDIR}/uefi-certificates/db.key"
SBSIGN_CERT = "${UNPACKDIR}/uefi-certificates/db.crt"

GRUB_PREFIX_DIR ?= "/EFI/BOOT"
EFI_BOOT_PATH ?= "/boot/efi/EFI/BOOT"

do_mkimage() {
    install -d "${D}${EFI_BOOT_PATH}"
    install -m 0600 "${UNPACKDIR}/grub-initial.cfg" "${D}${EFI_BOOT_PATH}/grub.cfg"

    grub-mkimage --disable-shim-lock \
        --prefix="${GRUB_PREFIX_DIR}" \
        --format="${GRUB_TARGET}-efi" \
        --directory="${B}/grub-core" \
        --output="${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}" \
        ${GRUB_BUILDIN}
}

fakeroot do_sign() {
    "${STAGING_BINDIR_NATIVE}/sbsign" \
        --key "${SBSIGN_KEY}" \
        --cert "${SBSIGN_CERT}" \
        "${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}" \
        --output "${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}.signed"

   install -m 0644 "${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}.signed" "${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}"

   install -d "${D}${EFI_BOOT_PATH}"
   install -m 0644 "${B}/${GRUB_IMAGE_PREFIX}${GRUB_IMAGE}.signed" "${D}${EFI_BOOT_PATH}/${GRUB_IMAGE}"
}

addtask sign after do_install before do_deploy do_package

FILES:${PN} += "${EFI_BOOT_PATH}"

CONFFILES:${PN} += "${EFI_BOOT_PATH}/grub.cfg"
