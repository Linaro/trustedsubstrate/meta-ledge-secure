FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

EXTRA_OEMAKE:append_armv7a = " COMPILE_NS_USER=32"
EXTRA_OEMAKE:append_armv7e = " COMPILE_NS_USER=32"

# for libgcc.a
EXTRA_OEMAKE:append = " LIBGCC_LOCATE_CFLAGS=--sysroot=${STAGING_DIR_HOST} \
                    CFG_PKCS11_TA=y \
                    "
