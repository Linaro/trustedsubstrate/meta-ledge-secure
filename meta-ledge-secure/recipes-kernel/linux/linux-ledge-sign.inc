DEPENDS += "e2fsprogs-native"

SRC_URI += "file://uefi-certificates/db.key"
SRC_URI += "file://uefi-certificates/db.crt"
SRC_URI += "file://uefi-certificates/db.auth"
SRC_URI += "file://uefi-certificates/PK.auth"
SRC_URI += "file://uefi-certificates/KEK.auth"

inherit kernel
inherit siteinfo
inherit sbsign

SBSIGN_KEY = "${UNPACKDIR}/uefi-certificates/db.key"
SBSIGN_CERT = "${UNPACKDIR}/uefi-certificates/db.crt"

# shell variable set inside do_compile task
SBSIGN_TARGET_BINARY = "$KERNEL_IMAGE"

do_compile:append() {
    KERNEL_IMAGE=$(find ${B} -name ${KERNEL_IMAGETYPE} -print -quit)
    do_sbsign
}

do_deploy:append() {
    KERNEL_IMAGE=$(find ${B} -name ${KERNEL_IMAGETYPE} -print -quit)

    install -m 0644 ${KERNEL_IMAGE}.signed ${DEPLOYDIR}/

    mkdir -p certimage
    cp ${UNPACKDIR}/uefi-certificates/PK.auth \
        ${UNPACKDIR}/uefi-certificates/KEK.auth \
        ${UNPACKDIR}/uefi-certificates/db.auth \
        ${UNPACKDIR}/uefi-certificates/db.crt \
        ./certimage

    ${STAGING_DIR_NATIVE}/usr/bin/truncate -s 4M certimage.ext4

    ${STAGING_DIR_NATIVE}/sbin/mkfs.ext4 certimage.ext4 -d ./certimage

    install -m 0644 certimage.ext4 ${DEPLOYDIR}/ledge-kernel-uefi-certs.ext4.img

    rm -rf ./certimage ./certimage.ext4
}
