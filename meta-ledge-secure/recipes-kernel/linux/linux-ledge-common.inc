inherit kernel siteinfo

DEPENDS += "coreutils-native"
DEPENDS += "sbsigntool-native"

# strip kernel modules before signing to reduce module size
EXTRA_OEMAKE += "INSTALL_MOD_STRIP=1"

FILESEXTRAPATHS:prepend := "${THISDIR}:${THISDIR}/files:${THISDIR}/linux-yocto:"

SRC_URI += "file://ledge-kmeta;type=kmeta;name=ledge-kmeta;destsuffix=ledge-kmeta"
SRC_URI:append:qemuarm =   " file://ledgeqemuarm-kmeta;type=kmeta;name=ledgeqemuarm-kmeta;destsuffix=ledgeqemuarm-kmeta"
SRC_URI:append:aarch64 = " file://ledgearm64-kmeta;type=kmeta;name=ledgearm64-kmeta;destsuffix=ledgearm64-kmeta"

# major feature config snippets
KERNEL_FEATURES += "cfg/net/ipv6.scc"
KERNEL_FEATURES += "cfg/virtio.scc"
KERNEL_FEATURES += "features/bluetooth/bluetooth.scc"
KERNEL_FEATURES += "features/media/media-all.scc"
KERNEL_FEATURES += "features/usb/serial-all.scc"
KERNEL_FEATURES += "features/wifi/wifi-all.scc"
KERNEL_FEATURES += "features/zram/zram.scc"
KERNEL_FEATURES += "fragment-2-secure.cfg"
KERNEL_FEATURES += "fragment-3-virtio.cfg"
KERNEL_FEATURES += "fragment-acpi.cfg"
KERNEL_FEATURES += "fragment-blk.cfg"
KERNEL_FEATURES += "fragment-composefs.cfg"
KERNEL_FEATURES += "fragment-debug.cfg"
# to identify different boards via /sys/class/dmi/id/board_vendor
KERNEL_FEATURES += "fragment-dmi.cfg"
KERNEL_FEATURES += "fragment-drm.cfg"
KERNEL_FEATURES += "fragment-powersave.cfg"
KERNEL_FEATURES += "fragment-seccomp.cfg"
KERNEL_FEATURES += "fragment-security.cfg"
KERNEL_FEATURES += "fragment-tee.cfg"

# board/HW specific config snippets
KERNEL_FEATURES:append:aarch64 = " fragment-ava.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-aws.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-imx.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-rockchip-media.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-rockchip.cfg"
KERNEL_FEATURES:append:aarch64 = " fragment-zynqmp.cfg"
KERNEL_FEATURES:append:qemuarm = " watchdog.cfg"

# use "defconfig" from kernel source arch/arm64/configs, then add our config fragments on top
KBUILD_DEFCONFIG:aarch64 = "defconfig"
# from kernel source arch/arm/configs to follow upstream defaults with new kernel versions
KBUILD_DEFCONFIG:arm = "multi_v7_defconfig"
# apply upstream defaults after defconfig and config fragments, to support new kernel
# features and defaults automatically
KCONFIG_MODE = "alldefconfig"

SRC_URI += " \
    file://0001-rk3399-rock-pi-4.dtsi-enable-imx219-isp.patch \
"

do_compile:append:aarch64() {
    oe_runmake -C ${B} dtbs
}

do_compile:append:qemuarm() {
    oe_runmake -C ${B} dtbs
}

do_install:append:aarch64() {
    oe_runmake -C ${B} DEPMOD=echo INSTALL_DTBS_PATH=${D}/boot/dtb dtbs_install
}

do_install:append:qemuarm() {
    oe_runmake -C ${B} DEPMOD=echo INSTALL_DTBS_PATH=${D}/boot/dtb dtbs_install
}

do_install:append() {
    if [ ! -d ${D}/boot/dtb ]; then
        # force the creation of dtb directory on boot to have
        install -d ${D}/boot/dtb
        echo "Empty content on case there is no devicetree" > ${D}/boot/dtb/.emtpy
    fi

    #rename device tree
    for dtb in ${DTB_RENAMING}
    do
        dtb_orignal=$(echo $dtb | cut -d':' -f 1 )
        dtb_renamed=$(echo $dtb | cut -d':' -f 2 )

        if [ -f ${D}/boot/$dtb_orignal ]; then
            cd ${D}/boot/
            ln -s $dtb_orignal $dtb_renamed
           cd -
        fi
        if [ -f ${D}/boot/dtb/$dtb_orignal ]; then
            cd ${D}/boot/dtb/
            ln -s $dtb_orignal $dtb_renamed
            cd -
        fi
    done
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}/dtb
    if [ -d ${WORKDIR}/package/boot/dtb ];then
        cp -rf ${WORKDIR}/package/boot/dtb ${DEPLOYDIR}/
    fi
}

FILES:${KERNEL_PACKAGE_NAME}-base += "/${KERNEL_IMAGEDEST}/dtb"
FILES:${KERNEL_PACKAGE_NAME}-base += "${nonarch_base_libdir}/modules/${KERNEL_VERSION}/modules.builtin.modinfo "
FILES:${KERNEL_PACKAGE_NAME}-base += "${base_libdir}/modprobe.d"
