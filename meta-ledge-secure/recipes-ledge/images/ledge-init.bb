SUMMARY = "Basic init for initramfs to mount and pivot root"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "\
    file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
"

PV="1.0"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://init.ledge "
SRC_URI += "file://signing_key_public.pem"
SRC_URI += "file://default.pcr.signature"
SRC_URI += "file://kernel515.pcr.signature"

inherit deploy

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

do_install() {
    install -m 0755 ${UNPACKDIR}/init.ledge ${D}/init

    # Create device nodes expected by kernel in initramfs
    # before executing /init.
    install -d "${D}/dev"
    install -d "${D}/run"
    mknod -m 0600 "${D}/dev/console" c 5 1
    mkdir -p ${D}/tpm2
    install -m 0755 ${UNPACKDIR}/signing_key_public.pem ${D}/tpm2/
}

do_deploy() {
    install -m 644 ${UNPACKDIR}/default.pcr.signature ${DEPLOYDIR}
    install -m 644 ${UNPACKDIR}/kernel515.pcr.signature ${DEPLOYDIR}
}
addtask deploy after do_install before do_build

FILES:${PN} += "/init"
FILES:${PN} += "/dev"
FILES:${PN} += "/run"
FILES:${PN} += "/tpm2"

RDEPENDS:${PN} += "bash"
RDEPENDS:${PN} += "coreutils"
RDEPENDS:${PN} += "e2fsprogs-resize2fs"
RDEPENDS:${PN} += "grep"
RDEPENDS:${PN} += "gawk"

# Required to grow root partition in the first boot
RDEPENDS:${PN} += "parted"

RDEPENDS:${PN} += "pigz"
RDEPENDS:${PN} += "${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'udev', 'eudev', d)}"
RDEPENDS:${PN} += "util-linux-blockdev"
RDEPENDS:${PN} += "util-linux-lsblk"
RDEPENDS:${PN} += "util-linux-mount"
